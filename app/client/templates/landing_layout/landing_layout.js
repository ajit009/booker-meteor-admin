/*****************************************************************************/
/* LandingLayout: Event Handlers */
/*****************************************************************************/
Template.landingLayout.events({
});

/*****************************************************************************/
/* LandingLayout: Helpers */
/*****************************************************************************/
Template.landingLayout.helpers({
});

/*****************************************************************************/
/* LandingLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.landingLayout.onCreated(function () {
});

Template.landingLayout.onRendered(function () {
});

Template.landingLayout.onDestroyed(function () {
});
