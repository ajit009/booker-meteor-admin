/*****************************************************************************/
/* PageHeader: Event Handlers */
/*****************************************************************************/
Template.pageHeader.events({

    'click .small-header-action': function(event){
        event.preventDefault();
        $('.normalheader').toggleClass('small-header');
        $('#hbreadcrumb').toggleClass('m-t-lg');
        $('.clip-header i').toggleClass('fa-arrow-up').toggleClass('fa-arrow-down');
    }

});

/*****************************************************************************/
/* PageHeader: Helpers */
/*****************************************************************************/
Template.pageHeader.helpers({
});

/*****************************************************************************/
/* PageHeader: Lifecycle Hooks */
/*****************************************************************************/
Template.pageHeader.onCreated(function () {
});

Template.pageHeader.onRendered(function () {
});

Template.pageHeader.onDestroyed(function () {
});
