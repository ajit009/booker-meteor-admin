/*****************************************************************************/
/* Splash: Event Handlers */
/*****************************************************************************/
Template.splash.events({
});

/*****************************************************************************/
/* Splash: Helpers */
/*****************************************************************************/
Template.splash.helpers({
});

/*****************************************************************************/
/* Splash: Lifecycle Hooks */
/*****************************************************************************/
Template.splash.onCreated(function () {
});

Template.splash.onRendered(function () {
});

Template.splash.onDestroyed(function () {
});
