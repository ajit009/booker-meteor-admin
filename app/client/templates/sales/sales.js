/*****************************************************************************/
/* Sales: Event Handlers */
/*****************************************************************************/
Template.Sales.events({
	
	'click .reactive-table tbody tr': function (event, tem) {
        var csale   =   Inventory.findOne({itemName: this.itemName});

        $('#salesTable tbody').append('<tr><td><a href="#" id="prodName_' + count + '" data-type="text" data-pk="1" data-title="Enter name" class="editable editable-click"></a><td><a href="#" id="prodQuantity" data-type="text" data-pk="{{csale._id}}" data-title="Enter quantity" class="editable editable-click"></a></td><td><a href="#" id="prodSellingPrice" data-type="text" data-pk={{csale._id}} data-title="Enter selling price" class="editable editable-click"></a></td><td><a href="#" id="prodDiscounts" data-type="text" data-pk={{csale._id}} data-title="Enter discounts" class="editable editable-click"></a></td><td><button id="remove" onclick="deleteRow(this)" class="remove btn btn-warning btn-xs">Remove</button></td></tr>');

        $('#salesTable tbody tr:last  td:nth-child(2) a').editable({
            validate: function (value) {
                if ($.trim(value) == '') return 'This field is required';
                if($.trim(value)==csale.itemQuantity) confirm("Warning! You have added all available quantity");
            }
        });

        $('#salesTable tbody tr:last  td:nth-child(3) a').editable({
            url: '#',
            type: 'text',
            pk: 1,
            name: 'prodSellingPrice',
            title: 'Enter price'
        });

        $('#salesTable tbody tr:last  td:nth-child(4) a').editable({
            url: '#',
            type: 'text',
            pk: 1,
            name: 'prodDiscounts',
            title: 'Enter discount'
        });

        $('#salesTable tbody tr:last  td:nth-child(1) a ').text(csale.itemName);
        $('#salesTable tbody tr:last  td:nth-child(2) a ').text(0);
        $('#salesTable tbody tr:last  td:nth-child(3) a ').text(csale.itemSellingPrice);
        $('#salesTable tbody tr:last  td:nth-child(4) a ').text(csale.discounts);


    }

});

Template.Sales.helpers({
	settings: function () {
        return {
            collection: Inventory,
            rowsPerPage: 5,
            showFilter: true,
            fields: [
            	{ key: 'itemName', label: 'Name' },
			    { key: 'itemSellingPrice', label: 'Cost(includes GST)' },
			    { key: 'units', label: 'In Stock' },
                {key: 'notes', label: 'Instructions'}
            	]
        };
    },
    saleList : function() {
        return sale;
    },
    totalCost : function(){
        
    },
    deleteRow : function (btn) {
        var row = btn.parentNode.parentNode;
        row.parentNode.removeChild(row);
    }

});



function setEditable() {
    $.fn.editable.defaults.mode = 'inline';

    // Defaults
    $.fn.editable.defaults.url = "#";

    //$('#salesTable tbody tr:last  td:nth-child(1) a' ).editable({
    //    url: '#',
    //    type: 'text',
    //    pk: 1,
    //    name: "prodName_"+count ,
    //    title: 'Enter name'
    //});

    $('#prodQuantity').editable({
        validate: function (value) {
            if ($.trim(value) == '') return 'This field is required';
        }
    });

    $('#prodSellingPrice').editable({
        url: '#',
        type: 'text',
        pk: 1,
        name: 'prodSellingPrice',
        title: 'Enter price'
    });

    $('#prodDiscounts').editable({
        url: '#',
        type: 'text',
        pk: 1,
        name: 'prodDiscounts',
        title: 'Enter discount'
    });
}

/*****************************************************************************/
/* Sales: Lifecycle Hooks */
/*****************************************************************************/
Template.Sales.onCreated(function () {
    count	=	0;
});

Template.Sales.onRendered(function () {
	sale	=	[];
    //setEditable();
});

Template.Sales.onDestroyed(function () {
});
