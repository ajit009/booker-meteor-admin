/*****************************************************************************/
/* Header: Event Handlers */
/*****************************************************************************/
Template.header.events({

    'click .hide-menu': function (event) {

        event.preventDefault();

        if ($(window).width() < 769) {
            $("body").toggleClass("show-sidebar");
        } else {
            $("body").toggleClass("hide-sidebar");
        }
    },

    'click .right-sidebar-toggle': function (event) {
        event.preventDefault();
        $('#right-sidebar').toggleClass('sidebar-open');
    },

    'click #logoutLink': function (event, template){
        alert('Really! log out?');
        Meteor.logout(function(err) {
          // callback
          Session.set("ses",false);
        });
    }
});

/*****************************************************************************/
/* Header: Helpers */
/*****************************************************************************/
Template.header.helpers({
});

/*****************************************************************************/
/* Header: Lifecycle Hooks */
/*****************************************************************************/
Template.header.onCreated(function () {
});

Template.header.onRendered(function () {
});

Template.header.onDestroyed(function () {
});
