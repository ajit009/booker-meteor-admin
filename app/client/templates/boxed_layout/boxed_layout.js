/*****************************************************************************/
/* BoxedLayout: Event Handlers */
/*****************************************************************************/
Template.boxedLayout.events({
});

/*****************************************************************************/
/* BoxedLayout: Helpers */
/*****************************************************************************/
Template.boxedLayout.helpers({
});

/*****************************************************************************/
/* BoxedLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.boxedLayout.onCreated(function () {
});

Template.boxedLayout.onRendered(function () {
});

Template.boxedLayout.onDestroyed(function () {
});
