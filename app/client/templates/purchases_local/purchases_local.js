/*****************************************************************************/
/* PurchasesLocal: Event Handlers */
/*****************************************************************************/
Template.purchases_local.events({
	'submit #purchase-local-form': function(e,t){
		e.preventDefault();
		var iName	=	t.find('#itemName').value;
		var iCat	=	t.find('#itemCategory').value;
		var iPp	=	t.find('#itemPp').value;
		var iSp	=	t.find('#itemSp').value;
		var ivendorName	=	t.find('#itemVendor').value;
		var inotes	=	t.find('#notes').value;
		var iDisconuts	=	t.find('#discounts').value;
		var oldUnits	=	parseInt(t.find('#itemUnits').value);
		var newU	=	 + parseInt(t.find('#newUnits').value) ;
		var updateUnits	=	oldUnits+newU;
		var purchaseC 	=	newU*parseInt(iPp);
		
		Inventory.update(this._id, {
			$set: {
			itemPurchasePrice: iPp,
			itemSellingPrice: iSp,
			itemVendor: ivendorName,
			notes : inotes,
			discounts: iDisconuts,
			units: updateUnits,
			createAt: new Date

		}}, function(err){

			if (err){
				alert(err);
			}else{
					
			}
		});	
		Purchases.insert({
					itemName : iName,
					itemCategory: iCat,
					itemPurchasePrice: iPp,
					itemSellingPrice: iSp,
					itemVendor: ivendorName,
					addedUnits : newU,
					discounts: iDisconuts,
					newUnits: updateUnits,
					purchaseCost: purchaseC,
					createAt: new Date

				}, function(err1){

					if (err1){
						alert(err1);
					}else{
						alert('Updated record successfully');
					}
				});	
		Router.go('/inventory');
		

	},

	'click #cancel' : function(){
		Router.go('/inventory');
	}

	
	
});

/*****************************************************************************/
/* PurchasesLocal: Helpers */
/*****************************************************************************/
Template.purchases_local.helpers({

});

/*****************************************************************************/
/* PurchasesLocal: Lifecycle Hooks */
/*****************************************************************************/
Template.purchases_local.onCreated(function () {
});

Template.purchases_local.onRendered(function () {

});

Template.purchases_local.onDestroyed(function () {
});
