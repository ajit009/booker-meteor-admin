/*****************************************************************************/
/* Login: Event Handlers */
/*****************************************************************************/
Template.login.events({
	'submit #loginForm' : function(e, t){
      e.preventDefault();
      // retrieve the input field values
      var email = t.find('#username').value
        , password = t.find('#password').value;

        Meteor.loginWithPassword(email, password, function(err){
        if (err) {
        	alert("login failed");
        }else{
          Router.go('/dashboard');
        }
      });
         return '/dashboard'; 
      }
});

/*****************************************************************************/
/* Login: Helpers */
/*****************************************************************************/
Template.login.helpers({
});

/*****************************************************************************/
/* Login: Lifecycle Hooks */
/*****************************************************************************/
Template.login.onCreated(function () {
});

Template.login.onRendered(function () {
});

Template.login.onDestroyed(function () {
});
