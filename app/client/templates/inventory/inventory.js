/*****************************************************************************/
/* Inventory: Event Handlers */
/*****************************************************************************/
Template.Inventory.events({
	'submit #inventory-form': function(e,t){
		e.preventDefault();
		var iName	=	t.find('#itemName').value;
		var iCat	=	t.find('#itemCategory').value;
		var iPp	=	t.find('#itemPp').value;
		var val =	parseInt(t.find('#itemSp').value); 
		//processing GST as a hardcoded value
		var iSp	=	val+(val*0.1);
		var ivendorName	=	t.find('#itemVendor').value;
		var inotes	=	t.find('#notes').value;
		var iDisconuts	=	t.find('#discounts').value;

		Inventory.insert({
			itemName : iName,
			itemCategory: iCat,
			itemPurchasePrice: iPp,
			itemSellingPrice: iSp,
			itemVendor: ivendorName,
			notes : inotes,
			discounts: iDisconuts,
			units: 0,
			createAt: new Date

		}, function(err){

			if (err){
				alert(err);
			}else{
				alert(iName +' added to stocks.');
			}
		});	

	},

	"click #delete": function () {
		alert('deleting Record '+ this.itemName);
      Inventory.remove(this._id);
    }
});

/*****************************************************************************/
/* Inventory: Helpers */
/*****************************************************************************/
Template.Inventory.helpers({
	item : function(){

		return Inventory.find().fetch();
	}
});

/*****************************************************************************/
/* Inventory: Lifecycle Hooks */
/*****************************************************************************/
Template.Inventory.onCreated(function () {
});

Template.Inventory.onRendered(function () {
	$('#example1').footable();
});

Template.Inventory.onDestroyed(function () {
});
