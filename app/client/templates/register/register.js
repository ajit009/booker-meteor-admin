/*****************************************************************************/
/* Register: Event Handlers */
/*****************************************************************************/
Template.register.events({
	'submit #registerForm' : function(e, t){
      e.preventDefault();
      var _email = t.find('#inputEmail').value;
      var _password = t.find('#inputPassword').value;
      var _name	=	t.find('#inputName').value;

        Accounts.createUser({ 
        	username: _name, 
        	email : _email,
        	password: _password

        }, function(err){
        if (err) {
        	alert("Registration failed");
        }else{
          Router.go('/dashboard');
        }
      });

        t.find('#inputEmail').value='';
      	t.find('#inputPassword').value='';
      	t.find('#inputName').value='';
         return false; 
         
      }
});

/*****************************************************************************/
/* Register: Helpers */
/*****************************************************************************/
Template.register.helpers({
});

/*****************************************************************************/
/* Register: Lifecycle Hooks */
/*****************************************************************************/
Template.register.onCreated(function () {
});

Template.register.onRendered(function(){

    // Initialize iCheck plugin
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

});

Template.register.onDestroyed(function () {
});
