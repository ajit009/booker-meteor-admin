/*****************************************************************************/
/* Purchases: Event Handlers */
/*****************************************************************************/
Template.Purchases.events({
	 'click .reactive-table tbody tr': function (event) {
    // 'click #mainTab' : function (event) {
    var purchase = this;
    var _id=	purchase.id;
    Router.go('/purchases/'+purchase._id);

  }
});

/*****************************************************************************/
/* Purchases: Helpers */
/*****************************************************************************/
Template.Purchases.helpers({
  
  purchaseCollection : function(){

		return Purchases.find().fetch();
	},

	settings: function () {
        return {
            collection: Inventory,
            rowsPerPage: 5,
            showFilter: true,
            fields: [
            	{ key: 'itemName', label: 'Name' },
			    { key: 'itemCategory', label: 'Category' },
			    { key: 'units', label: 'In Stock' }
            	]
        };
    },
    settingsForEvents: function () {
        return {
            collection: Purchases,
            rowsPerPage: 5,
            showFilter: true
            
        };
    }


});

/*****************************************************************************/
/* Purchases: Lifecycle Hooks */
/*****************************************************************************/
Template.Purchases.onCreated(function () {
});

Template.Purchases.onRendered(function () {
	var doughnutData = [
        {
            value: 20,
            color:"#62cb31",
            highlight: "#57b32c",
            label: "App"
        },
        {
            value: 120,
            color: "#91dc6e",
            highlight: "#57b32c",
            label: "Software"
        },
        {
            value: 100,
            color: "#a3e186",
            highlight: "#57b32c",
            label: "Laptop"
        }
    ];

    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true
    };

	var ctx = document.getElementById("doughnutChart").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);

    
});

Template.Purchases.onDestroyed(function () {
});
