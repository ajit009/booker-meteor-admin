Inventory = new Mongo.Collection('inventory');


if (Meteor.isServer) {
  Inventory.allow({
    insert: function (userId, doc) {
      return Meteor.user();
    },

    update: function (userId, doc, fieldNames, modifier) {
      return Meteor.user();
    },

    remove: function (userId, doc) {
      return Meteor.user();
    }
  });

  
}
