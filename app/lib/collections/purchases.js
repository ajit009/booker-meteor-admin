Purchases = new Mongo.Collection('purchases');


if (Meteor.isServer) {
  Purchases.allow({
    insert: function (userId, doc) {
      return Meteor.user();
    },

    update: function (userId, doc, fieldNames, modifier) {
      return Meteor.user();
    },

    remove: function (userId, doc) {
      return Meteor.user();
    }
  });

 
}
