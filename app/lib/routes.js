Router.configure({
    layoutTemplate: 'layout',
    notFoundTemplate: 'notFound'
});

//
// Dashboard route
//

Router.route('/', function () {
   
        Router.go('landingPage');
    
});

Router.route('/dashboard', function () {
    if (Meteor.user()){
       this.render('dashboard');
    }else {
        Router.go('login');
    }
    
});



Router.route('/register', function () {
    this.render('register');
    this.layout('blankLayout');
});

Router.route('/lock', function () {
    this.render('lock');
    this.layout('blankLayout');
});



//
// Landing page route
//

Router.route('/landingPage', function () {
    this.render('landingPage');
    this.layout('landingLayout');
});

//
// Global - Remove splash screen after after rendered layout
//

Router.onAfterAction(function()
{
    setTimeout(function()
    {
        $('.splash').css('display', 'none')
    })
});

Router.route('/login', function () {
  this.render('login');
  this.layout('blankLayout');},{
  controller: 'LoginController',
  where: 'client'    
 });


Router.route('inventory', {
  name: 'inventory',
  controller: 'InventoryController',
  where: 'client'
});

Router.route('/purchases/:_id', function () {
    this.render('purchases_local');    
}, {
    
    controller: 'PurchasesLocalController',
    where: 'client'
});



Router.route('purchases', {
  name: 'purchases',
  controller: 'PurchasesController',
  where: 'client'
});

Router.route('sales', {
  name: 'sales',
  controller: 'SalesController',
  where: 'client'
});