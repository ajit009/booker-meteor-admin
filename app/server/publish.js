




Meteor.publish('purchases_local', function () {
  return PurchasesLocal.find();
});

Meteor.publish('purchases', function () {
  return Purchases.find();
});

Meteor.publish('sales', function () {
  return Sales.find();
});

Meteor.publish('inventory', function () {
  return Inventory.find();
});